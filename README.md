# IntegratedThompsonThemeBundle

## Installation

composer.json

    "integrated/thompson-theme-bundle": "dev-master",

AppKernel.php

    $bundles = array(
        ...
        new Integrated\Bundle\ThompsonThemeBundle\IntegratedThompsonThemeBundle(),
        ...
    );


## Configuration

config.yml

    integrated_theme:
        themes:
            thompson:
                paths:
                    - @IntegratedThompsonThemeBundle/Resources/views/themes/thompson
                fallback:
                    - default
            ...
                    
    sp_bower:
        bundles:
            IntegratedThompsonThemeBundle:
                cache:
                    %kernel.cache_dir%/../sp_bower/IntegratedThompsonThemeBundle
            ...

