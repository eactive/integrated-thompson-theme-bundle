<?php

namespace Integrated\Bundle\ThompsonThemeBundle\Twig;

use Solarium\QueryType\Select\Result\Document;

/**
 * @author Jeroen van Leeuwen <jeroen@e-active.nl>
 */
class LabelExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('label', array($this, 'labelFilter'))
        ];
    }

    /**
     * @param Document $document
     * @return null|string
     */
    public function labelFilter(Document $document)
    {
        foreach ($document as $key => $value) {
            if (preg_match('/^taxonomy_(.*)_string$/', $key)) {
                if (is_array($value)) {
                    return $value[0];
                }

                return $value;
            }
        }

        return null;
    }
}
