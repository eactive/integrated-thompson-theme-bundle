<?php

namespace Integrated\Bundle\ThompsonThemeBundle\Twig;

/**
 * @author Jeroen van Leeuwen <jeroen@e-active.nl>
 */
class DateExtension extends \Twig_Extension
{
    /**
     * @var array
     */
     protected $format = array(
        'time' => 'short',
        'date' => 'medium',
        'custom' => 'd MMMM Y',
        'timeafterdate' => 'none',
    );

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('thompson_date', array($this, 'dateFilter'), ['needs_environment' => true])
        ];
    }

    /**
     * @param \Twig_Environment $twig
     * @param mixed $date
     * @param array $format
     * @return string
     */
    public function dateFilter(\Twig_Environment $twig, $date, array $format = []) {
        if (!$date instanceof \DateTime) {
            $date = new \DateTime($date);
        }

        $format = array_merge($this->format, $format);
        $filter = $twig->getFilter('localizeddate');
        $now = new \DateTime();

        if ($date->format('Ymd') == $now->format('Ymd')) {
            return call_user_func($filter->getCallable(), $twig, $date, 'none', $format['time']);
        }
        
        if ($date->format('Hi') == '0000') {
            $format['timeafterdate'] = 'none';
            $format['custom'] = str_replace('H:mm', '', $format['custom']);
        }

        return call_user_func($filter->getCallable(), $twig, $date, $format['date'], $format['timeafterdate'], null, null, $format['custom']);
    }
}
