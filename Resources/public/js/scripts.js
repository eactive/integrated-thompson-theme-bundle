//Substitute SVG with PNG for non-svg browsers
if (!Modernizr.svg) {
	$('.svg-img').each(function(){
		$(this).attr('src', ($(this).attr('data-png-src')));
	});
}

$(document).ready(function(){

	//Placeholders Fix
	$('input, textarea').placeholder();

	$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		$(this).parent().siblings().removeClass('open');
		$(this).parent().toggleClass('open');
	});

	//Owl news carousel

	$("#big-carousel").owlCarousel({
	    autoPlay: false, //Set AutoPlay to 3 seconds
	    items : 1,
	    responsive: true,
	    navigation: true,
	    singleItem: true,
	    scrollPerPage: true,
	    navigationText: false,
	    slideSpeed: 1000,
	    autoHeight: true
	});

	//Video carousel
	$("#video-carousel").owlCarousel({
	    autoPlay: 3000, //Set AutoPlay to 3 seconds
	    items : 1,
	    responsive: true,
	    navigation: true,
	    singleItem: true,
	    scrollPerPage: true,
	    navigationText: false,
	    slideSpeed: 1000,
	    autoHeight: true
	});

	//Thumbnails carousel
	var thumbnail1 = $("#thumbnail1");
  	var thumbnail2 = $("#thumbnail2");

	thumbnail1.owlCarousel({
		autoPlay: 5000, //Set AutoPlay to 3 seconds
		autoHeight: true,
	    singleItem : true,
	    slideSpeed : 1000,
	    navigation: false,
	    pagination:false,
	    afterAction : thumbnailPosition,
	    responsiveRefreshRate : 200,
	});

	thumbnail2.owlCarousel({
	    items : 15,
	    itemsDesktop      : [1199,10],
	    itemsDesktopSmall     : [979,10],
	    itemsTablet       : [768,8],
	    itemsMobile       : [479,4],
	    pagination:false,
	    responsiveRefreshRate : 100,
	    afterInit : function(el){
	      el.find(".owl-item").eq(0).addClass("active");
	    }
	});

	function thumbnailPosition(el){
	    var current = this.currentItem;
	    $("#thumbnail2")
	      .find(".owl-item")
	      .removeClass("active")
	      .eq(current)
	      .addClass("active")
	    if($("#thumbnail2").data("owlCarousel") !== undefined){
	      center(current)
	    }
	}

	$("#thumbnail2").on("click", ".owl-item", function(e){
	    e.preventDefault();
	    var number = $(this).data("owlItem");
	    thumbnail1.trigger("owl.goTo",number);
	});

	function center(number){
	    var thumbnail2visible = thumbnail2.data("owlCarousel").owl.visibleItems;
	    var num = number;
	    var found = false;
	    for(var i in thumbnail2visible){
	      if(num === thumbnail2visible[i]){
	        var found = true;
	      }
	    }

	    if(found===false){
	      if(num>thumbnail2visible[thumbnail2visible.length-1]){
	        thumbnail2.trigger("owl.goTo", num - thumbnail2visible.length+2)
	      }else{
	        if(num - 1 === -1){
	          num = 0;
	        }
	        thumbnail2.trigger("owl.goTo", num);
	      }
	    } else if(num === thumbnail2visible[thumbnail2visible.length-1]){
	      thumbnail2.trigger("owl.goTo", thumbnail2visible[1])
	    } else if(num === thumbnail2visible[0]){
	      thumbnail2.trigger("owl.goTo", num-1)
	    }

	};

	//Expanded list action
	$( ".list-expanded.parent li a" ).click(function(event) {
		event.preventDefault();
		$(this).toggleClass('active');
		$(this).next('.list-expanded.sub-list').slideToggle("slow");
	});
	$( ".sub-list li a" ).click(function(event) {
		event.preventDefault();
		$(this).next('.subsub-list').slideToggle("slow");
	});

    $('form[name="integrated_poll"]').on('change', function() {
        $('form[name="integrated_poll"]').submit();
    });
});

//Lightbox action
$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});


$(window).load(function(){

});


$(window).resize(function(){

});
