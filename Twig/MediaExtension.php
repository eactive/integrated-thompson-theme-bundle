<?php

namespace Integrated\Bundle\ThompsonThemeBundle\Twig;

use Integrated\Bundle\ContentBundle\Document\Content\Content;
use Integrated\Bundle\ContentBundle\Document\Content\Image;
use Integrated\Bundle\ContentBundle\Document\Content\Video;

/**
 * @author Jeroen van Leeuwen <jeroen@e-active.nl>
 */
class MediaExtension extends \Twig_Extension
{
    const RELATION_TYPE = 'embedded';

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('media', array($this, 'mediaFilter'))
        ];
    }

    /**
     * @param Content $content
     * @return Image[]
     */
    public function mediaFilter(Content $content)
    {
        $references = [];
        foreach ($content->getRelationsByRelationType(self::RELATION_TYPE) as $relation) {
            if ($relation->getRelationId() == '__editor_image') {
                //skip inline images
                continue;
            }
            foreach ($relation->getReferences() as $reference) {
                if ($reference instanceof Image) {
                    $references[$reference->getId()] = $reference;
                }
            }
        }

        return $references;
    }
}
